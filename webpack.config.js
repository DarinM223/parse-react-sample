const path = require('path')

module.exports = {
  entry: [
    './js/setup.js'
  ],
  output: {
    path: path.join(__dirname, '/public/build'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        loaders: ['babel'],
        exclude: /node_modules/,
        presets: ['es2015', 'react']
      }
    ]
  }
}
