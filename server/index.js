const express = require('express')
const Parse = require('parse/node')
const ParseServer = require('parse-server')

const SERVER_PORT = process.env.PORT || 8080
const APP_ID = process.env.APP_ID || 'my-app-id'

Parse.initialize(APP_ID)
